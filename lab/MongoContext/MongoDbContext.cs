﻿using lab.Interfaces;
using lab.MongoModels;
using MongoDB.Driver;

namespace lab.MongoContext
{
  public class MongoDbContext : IMongoDbContext
  {
    public MongoDbContext()
    {
      _mongoClient = new MongoClient("mongodb+srv://laba:labaadmin@mycluster.lo3xm.mongodb.net/Home?retryWrites=true&w=majority");

      _db = _mongoClient.GetDatabase("Home");
    }

    private IMongoDatabase _db { get; }
    private IMongoClient _mongoClient { get; }

    public IMongoCollection<MongoBuilding> CollectionBuildings => _db.GetCollection<MongoBuilding>("Building");

    public IMongoCollection<MongoUser> CollectionUsers => _db.GetCollection<MongoUser>("User");
  }
}