﻿using System;
using System.Collections.Generic;
using System.Linq;
using lab.EF;
using lab.Entities;
using lab.Memento;
using lab.MongoContext;
using lab.MongoModels;

namespace lab.Repositories
{
  public class UnitOfWork
  {
    private readonly DataContext Database;

    private bool disposed;
    private BuildingRepository buildingRepository;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private MongoDbContext mongoDb;
    private TypeRepository typeRepository;
    private Snapshot Snapshot;


    public UnitOfWork(string connection)
    {
      Database = ContextFactory.Create(connection);
      Snapshot = new Snapshot(Database);
    }

    public TypeRepository Type
    {
      get
      {
        if (typeRepository is null)
          typeRepository = new TypeRepository(Database);
        return typeRepository;
      }
    }

    public BuildingRepository Building
    {
      get
      {
        if (buildingRepository is null)
          buildingRepository = new BuildingRepository(Database);
        return buildingRepository;
      }
    }

    public UserRepository Users
    {
      get
      {
        if (userRepository is null)
          userRepository = new UserRepository(Database);
        return userRepository;
      }
    }

    public RoleRepository Role
    {
      get
      {
        if (roleRepository is null)
          roleRepository = new RoleRepository(Database);
        return roleRepository;
      }
    }

    public MongoDbContext MongoDb {
      get
      {
        if (mongoDb is null)
        {
          mongoDb = new MongoDbContext();
        }

        return mongoDb;
      }
    }

    public void Save()
    {
      Database.SaveChanges();
      Snapshot.Buildings = Database.Set<Building>();
    }

    public virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        if (disposing) Database.Dispose();
        disposed = true;
      }
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    public void ReplicateToMongo(MongoDbContext context)
    {
      MongoRepository _db = new MongoRepository(context);

      var items = this.Building.GetAll();

      foreach (var obj in items)
      {
        MongoBuilding item = new MongoBuilding
        {
          Address = obj.Address,
          Type = "someType",
          User = new List<User>(){new User{LastName = "Romankiv"}}
        };

        context.CollectionBuildings.InsertOne(item);
      }
    }

    public void ReplicateFromMongo(MongoDbContext context)
    {
      MongoRepository _db = new MongoRepository(context);
      IEnumerable<MongoBuilding> buildings = _db.GetAllBuildings();

      foreach (var obj in buildings)
      {
        Building building = new Building
        {
          Address = obj.Address,
          Type = this.Type.GetAll().First()
        };

        this.Building.Add(building);
      }
      this.Save();
    }
  }
}