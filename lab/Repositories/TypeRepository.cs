﻿using System;
using System.Collections.Generic;
using System.Linq;
using lab.EF;
using lab.Entities;
using lab.Interfaces;
using lab.Observer;
using lab.Observer.Listeners;

namespace lab.Repositories
{
  public class TypeRepository : IRepository<BuildingType>
  {
    private readonly DataContext Database;

    private readonly EventManager events = new EventManager();

    public TypeRepository(DataContext db)
    {
      Database = db;

      events.Attach(new TypeEntitieListener());
    }

    public async void Add(BuildingType item)
    {
      await Database.BuildingTypes.AddAsync(item);
      events.Notify("Add");
    }

    public void Delete(int Id)
    {
      var type = Database.BuildingTypes.Find(Id);
      if (type is null)
      {
        Database.BuildingTypes.Remove(type);
        return;
      }

      throw new NullReferenceException("There isn't type with this Id");
    }

    public BuildingType Get(int Id)
    {
      return Database.BuildingTypes.Find(Id);
    }

    public IEnumerable<BuildingType> GetAll()
    {
      return Database.BuildingTypes.ToList();
    }

    public BuildingType GetByName(string Name)
    {
      var types = Database.BuildingTypes.ToList();

      return types.Where(item => item.Name == Name).FirstOrDefault();
    }
  }
}