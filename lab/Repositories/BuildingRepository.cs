﻿using System;
using System.Collections.Generic;
using System.Linq;
using lab.EF;
using lab.Entities;
using lab.Interfaces;
using lab.Observer;
using lab.Observer.Listeners;

namespace lab.Repositories
{
  public class BuildingRepository : IRepository<Building>
  {
    private readonly DataContext Database;

    private readonly EventManager events = new EventManager();

    public BuildingRepository(DataContext db)
    {
      Database = db;

      events.Attach(new BuildingEntitieListener());
    }

    public void Add(Building item)
    {
      Database.Buildings.Add(item);

      events.Notify("Add");
    }


    public void Delete(int Id)
    {
      var someEvent = Database.Buildings.Find(Id);

      if (someEvent is null)
      {
        Database.Buildings.Remove(someEvent);
        return;
      }

      throw new NullReferenceException("There isn't event with this Id");
    }

    public Building Get(int Id)
    {
      return Database.Buildings.Find(Id);
    }

    public IEnumerable<Building> GetAll()
    {
      return Database.Buildings.ToList();
    }

    public Building GetByName(string Name)
    {
      var tasks = Database.Buildings.ToList();

      return tasks.Where(item => item.Address == Name).FirstOrDefault();
    }
  }
}