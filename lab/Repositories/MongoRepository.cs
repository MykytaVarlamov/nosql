﻿using System.Collections.Generic;
using lab.MongoContext;
using lab.MongoModels;
using MongoDB.Bson;
using MongoDB.Driver;

namespace lab.Repositories
{
  public class MongoRepository
  {
    private readonly MongoDbContext _db;

    public MongoRepository(MongoDbContext context)
    {
      _db = context;
    }

    public void AddEvent(MongoUser item)
    {
      _db.CollectionUsers.InsertOne(item);
    }

    public void AddSport(MongoBuilding item)
    {
      _db.CollectionBuildings.InsertOne(item);
    }

    public IEnumerable<MongoUser> GettAllUsers()
    {
      var events = _db.CollectionUsers.Find(new BsonDocument()).ToList();

      return events;
    }

    public IEnumerable<MongoBuilding> GetAllBuildings()
    {
      var buildings = _db.CollectionBuildings.Find(new BsonDocument()).ToList();

      return buildings;
    }

    public MongoBuilding GetEvent(string id)
    {
      var events = _db.CollectionBuildings.Find(new BsonDocument()).ToList();

      var filter = Builders<MongoBuilding>.Filter.Eq("_id", ObjectId.Parse(id));
      var item = _db.CollectionBuildings.Find(filter).FirstOrDefault();
      return item;
    }

    public MongoUser GetSport(string id)
    {
      var sports = _db.CollectionUsers.Find(new BsonDocument()).ToList();

      var filter = Builders<MongoUser>.Filter.Eq("_id", ObjectId.Parse(id));
      var item = _db.CollectionUsers.Find(filter).FirstOrDefault();
      return item;
    }
  }
}