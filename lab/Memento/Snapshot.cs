﻿using System;
using System.Collections.Generic;
using System.Text;
using lab.EF;
using lab.Entities;
using Microsoft.EntityFrameworkCore;

namespace lab.Memento
{
  public class Snapshot
  {
    public DataContext _db;
    private DbSet<Building> snapshotBuilding;

    public Snapshot(DataContext context)
    {
      context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
      _db = context;
      snapshotBuilding = _db.Set<Building>();
    }



    public DbSet<Building> Buildings
    {
      get => snapshotBuilding;
      set => snapshotBuilding = value;
    }
  }
}
