﻿using System;
using System.Collections.Generic;
using System.Text;
using lab.Entities;
using lab.Presentation;

namespace lab.Host
{
  class ViewControllerHost
  {
    private ViewController service;
    private User user;


    public ViewControllerHost(ViewController view)
    {
      this.service = view;
      //user = service.Database.Users.Get(4);
    }

    public void AllBuldings()
    {
      service.AllBuildings();
    }

    public void AllBuildingTypes()
    {
      service.AllBuildingTypes();
    }

    public void AllUsers()
    {
      service.AllUsers();
    }

    public void AddBuilding()
    {
      if (user.RoleId == 4)
      {
        service.AddBuilding();
      }
      else
      {
        Console.WriteLine("No access");
      }
    }

    public void DeleteBuilding()
    {
      if (user.RoleId == 4)
      {
        service.DeleteBuilding();
      }
      else
      {
        Console.WriteLine("No access");
      }
    }

    public void AddType()
    {
      if (user.RoleId == 4)
      {
        service.AddType();
      }
      else
      {
        Console.WriteLine("No access");
      }
    }

    public void DeleteType()
    {
      if (user.RoleId == 4)
      {
        service.DeleteType();
      }
      else
      {
        Console.WriteLine("No access");
      }
    }


    public void LogIn()
    {
      //this.user = service.LogIn();

    }
    public void LogOut()
    {
      ViewController view = new ViewController("DefaultConnection");
      view.Run();
    }

    public void Run()
    {


        ConsoleKeyInfo key1 = Console.ReadKey();
      ConsoleKeyInfo key2 = Console.ReadKey();
      Dictionary<ConsoleKey, Action> action = new Dictionary<ConsoleKey, Action>();

      if (key1.Key == ConsoleKey.G)
      {
        Console.WriteLine();
        action.Add(ConsoleKey.B, new Action(this.AllBuldings));
        action.Add(ConsoleKey.T, new Action(this.AllBuildingTypes));
        action.Add(ConsoleKey.E, new Action(this.AllUsers));
      }
      else if (key1.Key == ConsoleKey.L)
      {
        Console.WriteLine();
        this.LogIn();
        this.Run();
      }
      else if (key1.Key == ConsoleKey.O)
      {
        Console.WriteLine();
        this.LogOut();
        this.Run();
      }
      else if (key1.Key == ConsoleKey.A)
      {
        Console.WriteLine();
        action.Add(ConsoleKey.B, new Action(this.AddBuilding));
        action.Add(ConsoleKey.T, new Action(this.AddType));
      }
      else if (key1.Key == ConsoleKey.D)
      {
        Console.WriteLine();
        action.Add(ConsoleKey.B, new Action(this.DeleteBuilding));
        action.Add(ConsoleKey.T, new Action(this.DeleteType));
      }

      action[key2.Key].Invoke();

      this.Run();
    }
  }
}

