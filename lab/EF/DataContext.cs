﻿using lab.Entities;
using Microsoft.EntityFrameworkCore;

namespace lab.EF
{
  public class DataContext : DbContext
  {
    private static DataContext db;

    private DataContext(DbContextOptions<DataContext> options) : base(options)
    {
      Database.EnsureCreated();
    }

    public DbSet<User> Users { get; set; }
    public DbSet<BuildingType> BuildingTypes { get; set; }
    public DbSet<Building> Buildings { get; set; }
    public DbSet<Role> Roles { get; set; }

    public static DataContext GetContext(DbContextOptions<DataContext> options)
    {
      if (db is null)
        db = new DataContext(options);
      return db;
    }
  }
}