﻿using System;
using lab.Host;
using lab.MongoContext;
using lab.Presentation;
using lab.Repositories;

namespace lab
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      var context = new MongoDbContext();
      var _db = new MongoRepository(context);

      var users = _db.GettAllUsers();

      foreach (var item in users) Console.WriteLine(item.FirstName);

      ViewController view = new ViewController("DefaultConnection");
      ViewControllerHost host = new ViewControllerHost(view);
      view.Run();

      //UnitOfWork db = new UnitOfWork("DefaultConnection");
      //db.ReplicateFromMongo(context);
    }
  }
}