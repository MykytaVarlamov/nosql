﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using lab.Builders;
using lab.Entities;
using lab.Host;
using lab.MongoModels;
using lab.Repositories;

namespace lab.Presentation
{
  public class ViewController
  {
    public UnitOfWork Database;

    public ViewController(string stringconnection)
    {
      Database = new UnitOfWork(stringconnection);
    }

    private User user = null;
    public void LogIn()
    {
      Console.WriteLine("Enter user login :");
      string login = Console.ReadLine();


      user = Database.Users.GetAll()
        .Where(item => item.Login == login).FirstOrDefault();

      if (user is null)
      {
        Console.WriteLine("Wrong");
        throw new NullReferenceException();
      }
    }

    public void AllBuildings()
    {
      var buildings = Database.Building.GetAll();

      foreach (var item in buildings)
        Console.WriteLine(item.Id + "\t" + item.Address + "\t" +
                          "users number : " + item.Users.Count);
    }

    public void AllEventsTypes()
    {
      var types = Database.Type.GetAll();

      foreach (var item in types) Console.WriteLine(item.Id + "\t" + item.Name);
    }

    public void AllUsers()
    {
      var events = Database.Building.GetAll();

      foreach (var item in events)
        Console.WriteLine(item.Id + "\t" + item.Address + "\t" + item.Type.Name + "\t" +
                          "users number : " + item.Users.Count);
    }

    public void AddUsers()
    {
      Console.WriteLine("Enter user name : ");
      var name = Console.ReadLine();

      if (Database.Users.GetByName(name) != null)
      {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Such user exists");
        Console.ForegroundColor = ConsoleColor.White;
        return;
      }

      Database.Users.Add(new User {FirstName = name});
      Database.Save();
    }

    public void DeleteUser()
    {
      Console.WriteLine("Enter user name : ");

      var name = Console.ReadLine();

      if (Database.Users.GetByName(name) is null)
      {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Such User not exists");
        Console.ForegroundColor = ConsoleColor.White;
        return;
      }

      Database.Users.Delete(Database.Users.GetByName(name).Id);
      Database.Save();
    }

    public void AddType()
    {
      Console.WriteLine("Enter type name : ");
      var name = Console.ReadLine();

      if (Database.Type.GetByName(name) != null)
      {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Such event type exists");
        Console.ForegroundColor = ConsoleColor.White;
        return;
      }

      Database.Type.Add(new BuildingType {Name = name});
      Database.Save();
    }

    public void DeleteType()
    {
      Console.WriteLine("Enter type name : ");

      var name = Console.ReadLine();

      if (Database.Type.GetByName(name) is null)
      {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Such type not exists");
        Console.ForegroundColor = ConsoleColor.White;
        return;
      }

      Database.Type.Delete(Database.Type.GetByName(name).Id);
      Database.Save();
    }

    public void AddBuilding()
    {
      Console.WriteLine("Enter address name :");
      var name = Console.ReadLine();
      //Console.WriteLine("Enter type name : ");
      //var type = Console.ReadLine();
      var type = "Home";

      //if (Database.Building.GetByName(type) is null)
      //{
      //  Console.ForegroundColor = ConsoleColor.Red;
      //  Console.WriteLine("Such type not exists, try again");
      //  Console.ForegroundColor = ConsoleColor.White;
      //  return;
      //}

      var builder = new BuildingBuilder(Database);

      Console.WriteLine("Enter users number : ");

      var num = int.Parse(Console.ReadLine());

      Console.WriteLine("Enter user names : ");
      for (var i = 0; i < num; i++)
      {
        var item = Console.ReadLine();
        builder.AddUser(item);
      }

      builder.SetType(type);
      builder.SetAddress(name);
      MongoBuilding mongoBuilding = new MongoBuilding
      {
        Address = builder.build().Address,
        User = builder.build().Users
      }; 

      Database.MongoDb.CollectionBuildings.InsertOne(mongoBuilding);
      Database.Save();
    }

    public void DeleteBuilding()
    {
      Console.WriteLine("Enter event name : ");

      var name = Console.ReadLine();

      if (Database.Building.GetByName(name) is null)
      {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Such type not exists");
        Console.ForegroundColor = ConsoleColor.White;
        return;
      }

      Database.Building.Delete(Database.Building.GetByName(name).Id);
      Database.Save();
    }

    public void Run()
    {
      if (user != null)
      {
        ViewControllerHost host = new ViewControllerHost(this);
        host.Run();
      }
      var key1 = Console.ReadKey();
      //var key2 = Console.ReadKey();
      var action = new Dictionary<ConsoleKey, Action>();

      if (key1.Key == ConsoleKey.G)
      {
        action.Add(ConsoleKey.S, AllBuildings);
        action.Add(ConsoleKey.T, AllEventsTypes);
        action.Add(ConsoleKey.E, AllUsers);
      }
      else if (key1.Key == ConsoleKey.A)
      {
        //action.Add(ConsoleKey.S, AddSport);
        action.Add(ConsoleKey.A, AddBuilding);
        //action.Add(ConsoleKey.E, AddEvent);
      }
      else if (key1.Key == ConsoleKey.D)
      {
        action.Add(ConsoleKey.S, DeleteBuilding);
        action.Add(ConsoleKey.T, DeleteType);
        //action.Add(ConsoleKey.E, DeleteEvent);
      }
      else if (key1.Key == ConsoleKey.L)
      {
        action.Add(ConsoleKey.L, LogIn);
      }
      

      action[key1.Key].Invoke();

      this.Run();
    }

    public void AllBuildingTypes()
    {
      var types = Database.Type.GetAll();
      foreach (BuildingType buildingType in types)
      {
        Console.WriteLine($"Name : {buildingType.Name}\t Buildings count : {buildingType.Buildings.Count()}");
      }
    }
  }
}