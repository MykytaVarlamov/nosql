﻿using lab.Entities;
using lab.Repositories;

namespace lab.Builders
{
  public class BuildingBuilder
  {
    private readonly UnitOfWork Database;
    private readonly Building Building;

    public BuildingBuilder(UnitOfWork db)
    {
      Database = db;
      Building = new Building();
    }

    public void SetAddress(string Name)
    {
      Building.Address = Name;
    }

    public void SetType(string Name)
    {
      var type = Database.Type.GetByName(Name).Id;

      Building.TypeId = type;
    }

    public BuildingBuilder AddUser(string Name)
    {
      var user = Database.Users.GetByName(Name);
      Building.Users.Add(user);
      return this;
    }

    public Building build()
    {
      return Building;
    }
  }
}