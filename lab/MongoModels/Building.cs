﻿using System.Collections.Generic;
using lab.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace lab.MongoModels
{
  public class MongoBuilding
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Address { get; set; }
    public int TypeId { get; set; }
    public string Type { get; set; }
    public List<User> User { get; set; }
  }
}