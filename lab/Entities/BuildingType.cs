﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lab.Entities
{
  public class BuildingType
  {
    [Key] public int Id { get; set; }

    [Required] public string Name { get; set; }
    public List<Building> Buildings { get; set; }
  }
}