﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lab.Entities
{
  public class Building
  {
    [Key] public int Id { get; set; }

    [Required]
    public string Address { get; set; }
    public int TypeId { get; set; }
    public BuildingType Type { get; set; }

    public List<User> Users { get; set; } = new List<User>();
  }
}