﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lab.Entities
{
  public class User
  {
    [Key] public int Id { get; set; }

    [Required] public string FirstName { get; set; }
    [Required] public string LastName { get; set; }
    public string Login { get; set; }
    [Required] public DateTime DateOfBirth { get; set; }
    [Required] public string Password { get; set; }
    [Required] public string Email { get; set; }
    public int RoleId { get; set; }
    public Role Role { get; set; }


    public List<Building> Buildings { get; set; } = new List<Building>();
  }
}