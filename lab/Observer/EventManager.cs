﻿using System.Collections.Generic;
using lab.Interfaces;

namespace lab.Observer
{
  public class EventManager : ISubject
  {
    private readonly List<IObserver> _observers;

    public EventManager()
    {
      _observers = new List<IObserver>();
    }

    public void Attach(IObserver observer)
    {
      _observers.Add(observer);
    }

    public void Detach(IObserver observer)
    {
      _observers.Remove(observer);
    }

    public void Notify(string type)
    {
      foreach (var item in _observers) item.Update(type, this);
    }
  }
}